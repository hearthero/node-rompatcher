import { PatchFile } from '../PatchFile';

const IPS_MAGIC = 'PATCH';
const IPS_MAX_SIZE = 0x1000000; // 16 megabytes
const IPS_RECORD_RLE = 0x0000;
const IPS_RECORD_SIMPLE = 0x01;

interface IPSRecord {
  offset: number;
  type: number;
  length: number;
  data?: any;
  byte?: number;
}

export class IPS {
  records: IPSRecord[] = [];
  truncate: number = 0;

  addSimpleRecord(o: number, d: any[]) {
    this.records.push({ offset: o, type: IPS_RECORD_SIMPLE, length: d.length, data: d });
  }

  addRLERecord(o: number, l: number, b: number) {
    this.records.push({ offset: o, type: IPS_RECORD_RLE, length: l, byte: b });
  }

  toString(): string {
    let nSimpleRecords = 0;
    let nRLERecords = 0;

    for (const record of this.records) {
      if (record.type === IPS_RECORD_RLE) {
        nRLERecords++;
      } else {
        nSimpleRecords++;
      }
    }

    let s = `Simple records: ' + ${nSimpleRecords}\n
      RLE records: ${nRLERecords}\n
      Total records: ${this.records.length}`;

    if (this.truncate !== 0) {
      s += '\nTruncate at: 0x' + this.truncate.toString(16);
    }

    return s;
  }

  export(fileName: string): PatchFile {
    let patchFileSize = 5; // PATCH string
    for (const record of this.records) {
      if (record.type === IPS_RECORD_RLE) {
        patchFileSize += 3 + 2 + 2 + 1; // offset+0x0000+length+RLE byte to be written
      } else {
        patchFileSize += 3 + 2 + record.data.length; // offset+length+data
      }
    }
    patchFileSize += 3; // EOF string

    if (this.truncate) patchFileSize += 3; // truncate

    const tempFile = new PatchFile(patchFileSize);
    tempFile.fileName = fileName + '.ips';
    tempFile.writeString(IPS_MAGIC);

    for (const rec of this.records) {
      tempFile.writeU24(rec.offset);
      if (rec.type === IPS_RECORD_RLE) {
        tempFile.writeU16(0x0000);
        tempFile.writeU16(rec.length);
        tempFile.writeU8(rec.byte || 0);
      } else {
        tempFile.writeU16(rec.data.length);
        tempFile.writeBytes(rec.data);
      }
    }

    tempFile.writeString('EOF');

    return tempFile;
  }

  apply(romFile: PatchFile) {
    let tempFile: PatchFile;

    if (this.truncate) {
      tempFile = romFile.slice(0, this.truncate as number);
    } else {
      let newFileSize = romFile.fileSize;
      for (const rec of this.records) {
        if (rec.type === IPS_RECORD_RLE) {
          if (rec.offset + rec.length > newFileSize) {
            newFileSize = rec.offset + rec.length;
          }
        } else {
          if (rec.offset + rec.data.length > newFileSize) {
            newFileSize = rec.offset + rec.data.length;
          }
        }
      }

      if (newFileSize === romFile.fileSize) {
        tempFile = romFile.slice(0, romFile.fileSize);
      } else {
        tempFile = new PatchFile(newFileSize);
        romFile.copyToFile(tempFile, 0);
      }
    }

    romFile.seek(0);

    for (const record of this.records) {
      tempFile.seek(record.offset);

      if (record.type === IPS_RECORD_RLE) {
        for (let j = 0; j < record.length; j++) tempFile.writeU8(record.byte as number);
      } else {
        tempFile.writeBytes(record.data);
      }
    }

    return tempFile;
  }
}

export function parseIPSFile(file: PatchFile): IPS {
  const patchFile = new IPS();
  file.seek(5);

  while (!file.isEOF()) {
    const offset = file.readU24();

    if (offset === 0x454f46) {
      /* EOF */
      if (file.isEOF()) {
        break;
      } else if (file.offset + 3 === file.fileSize) {
        patchFile.truncate = file.readU24();
        break;
      }
    }

    const length = file.readU16();

    if (length === IPS_RECORD_RLE) {
      patchFile.addRLERecord(offset, file.readU16(), file.readU8());
    } else {
      patchFile.addSimpleRecord(offset, file.readBytes(length));
    }
  }
  return patchFile;
}

export function createIPSFromFiles(original: PatchFile, modified: PatchFile): IPS {
  const patch = new IPS();

  if (modified.fileSize < original.fileSize) {
    patch.truncate = modified.fileSize;
  }

  let previousRecord: IPSRecord = { type: 0xdeadbeef, offset: 0, length: 0, data: [] };

  while (!modified.isEOF()) {
    let b1 = original.isEOF() ? 0x00 : original.readU8();
    let b2 = modified.readU8();

    if (b1 !== b2) {
      let RLEmode = true;
      const differentData = [];
      const startOffset = modified.offset - 1;

      while (b1 !== b2 && differentData.length < 0xffff) {
        differentData.push(b2);
        if (b2 !== differentData[0]) RLEmode = false;

        if (modified.isEOF() || differentData.length === 0xffff) break;

        b1 = original.isEOF() ? 0x00 : original.readU8();
        b2 = modified.readU8();
      }

      // check if this record is near the previous one
      let distance = startOffset - (previousRecord.offset + previousRecord.length);
      if (
        previousRecord.type === IPS_RECORD_SIMPLE &&
        distance < 6 &&
        previousRecord.length + distance + differentData.length < 0xffff
      ) {
        if (RLEmode && differentData.length > 6) {
          // separate a potential RLE record
          original.seek(startOffset);
          modified.seek(startOffset);
          previousRecord = { type: 0xdeadbeef, offset: 0, length: 0, data: [] };
        } else {
          // merge both records
          while (distance--) {
            previousRecord.data.push(modified.buffer[previousRecord.offset + previousRecord.length]);
            previousRecord.length++;
          }
          previousRecord.data = previousRecord.data.concat(differentData);
          previousRecord.length = previousRecord.data.length;
        }
      } else {
        if (startOffset >= IPS_MAX_SIZE) {
          throw new Error('files are too big for IPS format');
        }

        if (RLEmode && differentData.length > 2) {
          patch.addRLERecord(startOffset, differentData.length, differentData[0]);
        } else {
          patch.addSimpleRecord(startOffset, differentData);
        }
        previousRecord = patch.records[patch.records.length - 1];
      }
    }
  }

  if (modified.fileSize > original.fileSize) {
    const lastRecord = patch.records[patch.records.length - 1];
    const lastOffset = lastRecord.offset + lastRecord.length;

    if (lastOffset < modified.fileSize) {
      patch.addSimpleRecord(modified.fileSize - 1, [0x00]);
    }
  }

  return patch;
}
