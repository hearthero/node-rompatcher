import { PatchFile } from '../../PatchFile';

export class UPSFile extends PatchFile {
  writeVLV(data: number) {
    while (1) {
      const x = data & 0x7f;
      data = data >> 7;
      if (data === 0) {
        this.writeU8(0x80 | x);
        break;
      }
      this.writeU8(x);
      data = data - 1;
    }
  }

  readVLV() {
    let data = 0;
    let shift = 1;

    while (1) {
      const x = this.readU8();

      if (x === -1) throw new Error("Can't read UPS VLV at 0x" + (this.offset - 1).toString(16));

      data += (x & 0x7f) * shift;
      if ((x & 0x80) !== 0) break;
      shift = shift << 7;
      data += shift;
    }
    return data;
  }

  static getVLVLength(data: number) {
    let len = 0;
    while (1) {
      data = data >> 7;
      len++;
      if (data === 0) {
        break;
      }
      data = data - 1;
    }
    return len;
  }
}
