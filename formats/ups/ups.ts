import { PatchFile } from '../../PatchFile';
import { crc32, padZeroes } from '../../crc';
import { UPSFile } from './UPSFile';

const UPS_MAGIC = 'UPS1';

interface UPSRecord {
  offset: number;
  XORdata: number[];
}

export class UPS {
  records: UPSRecord[] = [];
  sizeInput = 0;
  sizeOutput = 0;
  checksumInput = 0;
  checksumOutput = 0;

  addRecord(relativeOffset: number, d: number[]) {
    this.records.push({ offset: relativeOffset, XORdata: d });
  }

  toString() {
    let s = 'Records: ' + this.records.length;
    s += '\nInput file size: ' + this.sizeInput;
    s += '\nOutput file size: ' + this.sizeOutput;
    s += '\nInput file checksum: ' + padZeroes(this.checksumInput, 4);
    s += '\nOutput file checksum: ' + padZeroes(this.checksumOutput, 4);
    return s;
  }

  export(fileName: string): PatchFile {
    let patchFileSize = UPS_MAGIC.length; // UPS1 string
    patchFileSize += UPSFile.getVLVLength(this.sizeInput); // input file size
    patchFileSize += UPSFile.getVLVLength(this.sizeOutput); // output file size

    for (const record of this.records) {
      patchFileSize += UPSFile.getVLVLength(record.offset);
      patchFileSize += record.XORdata.length + 1;
    }
    patchFileSize += 12; // input/output/patch checksums

    const tempFile = new UPSFile(patchFileSize);
    tempFile.fileName = fileName + '.ups';
    tempFile.writeString(UPS_MAGIC);

    tempFile.writeVLV(this.sizeInput);
    tempFile.writeVLV(this.sizeOutput);

    for (const record of this.records) {
      tempFile.writeVLV(record.offset);
      tempFile.writeBytes(record.XORdata);
      tempFile.writeU8(0x00);
    }
    tempFile.littleEndian = true;
    tempFile.writeU32(this.checksumInput);
    tempFile.writeU32(this.checksumOutput);
    tempFile.writeU32(crc32(tempFile, 0, true));

    return tempFile as PatchFile;
  }

  validateSource(romFile: PatchFile, headerSize?: number) {
    return crc32(romFile, headerSize) === this.checksumInput;
  }

  apply(romFile: PatchFile, validate: boolean) {
    if (validate && !this.validateSource(romFile)) {
      throw new Error('error_crc_input');
    }

    /* copy original file */
    const tempFile = new PatchFile(this.sizeOutput);
    romFile.copyToFile(tempFile, 0, this.sizeInput);

    romFile.seek(0);

    for (const record of this.records) {
      tempFile.skip(record.offset);
      romFile.skip(record.offset);

      for (let j = 0; j < record.XORdata.length; j++) {
        tempFile.writeU8((romFile.isEOF() ? 0x00 : romFile.readU8()) ^ record.XORdata[j]);
      }
      tempFile.skip(1);
      romFile.skip(1);
    }

    if (validate && crc32(tempFile) !== this.checksumOutput) {
      throw new Error('error_crc_output');
    }

    return tempFile;
  }
}

export function parseUPSFile(file: UPSFile): UPS {
  const patch = new UPS();

  file.seek(UPS_MAGIC.length);

  patch.sizeInput = file.readVLV();
  patch.sizeOutput = file.readVLV();

  while (file.offset < file.fileSize - 12) {
    const relativeOffset = file.readVLV();

    const XORdifferences = [];
    while (file.readU8()) {
      XORdifferences.push(file._lastRead);
    }
    patch.addRecord(relativeOffset, XORdifferences);
  }

  file.littleEndian = true;
  patch.checksumInput = file.readU32();
  patch.checksumOutput = file.readU32();

  if (file.readU32() !== crc32(file, 0, true)) {
    throw new Error('error_crc_patch');
  }

  file.littleEndian = false;
  return patch;
}

export function createUPSFromFiles(original: PatchFile, modified: PatchFile): UPS {
  const patch = new UPS();

  patch.sizeInput = original.fileSize;
  patch.sizeOutput = modified.fileSize;

  let previousSeek = 1;
  while (!modified.isEOF()) {
    let b1 = original.isEOF() ? 0x00 : original.readU8();
    let b2 = modified.readU8();

    if (b1 !== b2) {
      const currentSeek = modified.offset;
      const XORdata = [];

      while (b1 !== b2) {
        XORdata.push(b1 ^ b2);

        if (modified.isEOF()) break;
        b1 = original.isEOF() ? 0x00 : original.readU8();
        b2 = modified.readU8();
      }

      patch.addRecord(currentSeek - previousSeek, XORdata);
      previousSeek = currentSeek + XORdata.length + 1;
    }
  }

  patch.checksumInput = crc32(original);
  patch.checksumOutput = crc32(modified);
  return patch;
}
