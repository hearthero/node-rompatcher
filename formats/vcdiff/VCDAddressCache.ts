/*
	ported from https://github.com/vic-alexiev/TelerikAcademy/tree/master/C%23%20Fundamentals%20II/Homework%20Assignments/3.%20Methods/000.%20MiscUtil/Compression/Vcdiff
	by Victor Alexiev (https://github.com/vic-alexiev)
*/

import { VCDIFFFile } from './VCDIFFFile';

export class VCDAddressCache {
  nearSize: number;
  sameSize: number;
  near: number[];
  same: number[];

  addressStream: VCDIFFFile | undefined;
  nextNearSlot = 0;

  constructor(nearSize: number, sameSize: number) {
    this.nearSize = nearSize;
    this.sameSize = sameSize;

    this.near = new Array(nearSize);
    this.same = new Array(sameSize * 256);
  }

  reset(addressStream: VCDIFFFile) {
    this.nextNearSlot = 0;
    this.near.fill(0);
    this.same.fill(0);

    this.addressStream = addressStream;
  }

  decodeAddress(here: number, mode: number) {
    let address = 0;

    if (this.addressStream) {
      if (mode === 0) {
        address = this.addressStream.read7BitEncodedInt();
      } else if (mode === 1) {
        address = here - this.addressStream.read7BitEncodedInt();
      } else if (mode - 2 < this.nearSize) {
        // near cache
        address = this.near[mode - 2] + this.addressStream.read7BitEncodedInt();
      } else {
        // same cache
        const m = mode - (2 + this.nearSize);
        address = this.same[m * 256 + this.addressStream.readU8()];
      }

      this.update(address);
    }
    return address;
  }

  update(address: number) {
    if (this.nearSize > 0) {
      this.near[this.nextNearSlot] = address;
      this.nextNearSlot = (this.nextNearSlot + 1) % this.nearSize;
    }

    if (this.sameSize > 0) {
      this.same[address % (this.sameSize * 256)] = address;
    }
  }
}
