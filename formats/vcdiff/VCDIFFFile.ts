/* VCDIFF module for RomPatcher.js v20181021 - Marc Robledo 2018 - http://www.marcrobledo.com/license */
/* File format specification: https://tools.ietf.org/html/rfc3284 */
/*
	Mostly based in:
	https://github.com/vic-alexiev/TelerikAcademy/tree/master/C%23%20Fundamentals%20II/Homework%20Assignments/3.%20Methods/000.%20MiscUtil/Compression/Vcdiff
	some code and ideas borrowed from:
	https://hack64.net/jscripts/libpatch.js?6
*/

import { PatchFile } from '../../PatchFile';
import { VCDIFF } from './VCDIFF';

// winIndicator
const VCD_SOURCE = 0x01;
const VCD_TARGET = 0x02;
const VCD_ADLER32 = 0x04;

export function parseVCDIFF(file: PatchFile) {
  return new VCDIFF(file);
}

export class VCDIFFFile extends PatchFile {
  constructor(patchFile: PatchFile, offset = 0) {
    super(patchFile, offset);
    this.fileSize = patchFile.fileSize;
    this.buffer = patchFile.buffer;
    this.offset = offset;
  }

  read7BitEncodedInt() {
    let num = 0;
    let bits = 0;

    do {
      bits = this.readU8();
      num = (num << 7) + (bits & 0x7f);
    } while (bits & 0x80);

    return num;
  }

  decodeWindowHeader() {
    const windowHeader: any = {
      indicator: this.readU8(),
      sourceLength: 0,
      sourcePosition: 0,
      adler32: false,
    };

    if (windowHeader.indicator & (VCD_SOURCE | VCD_TARGET)) {
      windowHeader.sourceLength = this.read7BitEncodedInt();
      windowHeader.sourcePosition = this.read7BitEncodedInt();
    }

    windowHeader.deltaLength = this.read7BitEncodedInt();
    windowHeader.targetWindowLength = this.read7BitEncodedInt();
    windowHeader.deltaIndicator = this.readU8(); // secondary compression: 1=VCD_DATACOMP,2=VCD_INSTCOMP,4=VCD_ADDRCOMP
    if (windowHeader.deltaIndicator !== 0) {
      throw new Error('unimplemented windowHeader.deltaIndicator:' + windowHeader.deltaIndicator);
    }

    windowHeader.addRunDataLength = this.read7BitEncodedInt();
    windowHeader.instructionsLength = this.read7BitEncodedInt();
    windowHeader.addressesLength = this.read7BitEncodedInt();

    if (windowHeader.indicator & VCD_ADLER32) {
      windowHeader.adler32 = this.readU32();
    }

    return windowHeader;
  }

  copyToFile2(target: PatchFile, targetOffset: number, len: number) {
    for (let i = 0; i < len; i++) {
      target.buffer[targetOffset + i] = this.buffer[this.offset + i];
    }
    this.skip(len);
  }
}
