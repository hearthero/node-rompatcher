import { PatchFile } from '../../PatchFile';
import { VCDAddressCache } from './VCDAddressCache';
import { VCDIFFFile } from './VCDIFFFile';
import { adler32 } from '../../crc';

// hdrIndicator
const VCD_DECOMPRESS = 0x01;
const VCD_CODETABLE = 0x02;
const VCD_APPHEADER = 0x04; // nonstandard?

// winIndicator
const VCD_SOURCE = 0x01;
const VCD_TARGET = 0x02;

/*
	build the default code table (used to encode/decode instructions) specified in RFC 3284
	heavily based on
	https://github.com/vic-alexiev/TelerikAcademy/blob/master/C%23%20Fundamentals%20II/Homework%20Assignments/3.%20Methods/000.%20MiscUtil/Compression/Vcdiff/CodeTable.cs
*/

const VCD_ADD = 1;
const VCD_COPY = 3;

const VCD_DEFAULT_CODE_TABLE = (() => {
  const entries = [];

  const empty = { type: 0, size: 0, mode: 0 };

  // 0
  entries.push([{ type: 2, size: 0, mode: 0 }, empty]);

  // 1,18
  for (let i = 0; i < 18; i++) {
    entries.push([{ type: VCD_ADD, size: i, mode: 0 }, empty]);
  }

  // 19,162
  for (let i = 0; i < 9; i++) {
    entries.push([{ type: VCD_COPY, size: 0, mode: i }, empty]);

    for (let j = 4; j < 19; j++) {
      entries.push([{ type: VCD_COPY, size: j, mode: i }, empty]);
    }
  }

  // 163,234
  for (let i = 0; i < 6; i++) {
    for (let add = 1; add < 5; add++) {
      for (let copySize = 4; copySize < 7; copySize++) {
        entries.push([
          { type: VCD_ADD, size: add, mode: 0 },
          { type: VCD_COPY, size: copySize, mode: i },
        ]);
      }
    }
  }

  // 235,246
  for (let i = 6; i < 9; i++) {
    for (let addSize = 1; addSize < 5; addSize++) {
      entries.push([
        { type: VCD_ADD, size: addSize, mode: 0 },
        { type: VCD_COPY, size: 4, mode: i },
      ]);
    }
  }

  // 247,255
  for (let i = 0; i < 9; i++) {
    entries.push([
      { type: VCD_COPY, size: 4, mode: i },
      { type: VCD_ADD, size: 1, mode: 0 },
    ]);
  }

  return entries;
})();

export class VCDIFF {
  file: PatchFile;

  constructor(patchFile: PatchFile) {
    this.file = patchFile;
  }

  toString() {
    return 'VCDIFF patch';
  }

  apply(romFile: PatchFile, validate: boolean) {
    const parser = new VCDIFFFile(this.file);
    parser.seek(4);
    let cache: VCDAddressCache;

    const headerIndicator = parser.readU8();

    if (headerIndicator & VCD_DECOMPRESS) {
      // has secondary decompressor, read its id
      const secondaryDecompressorId = parser.readU8();

      if (secondaryDecompressorId !== 0) throw new Error('not implemented: secondary decompressor');
    }

    if (headerIndicator & VCD_CODETABLE) {
      const codeTableDataLength = parser.read7BitEncodedInt();

      if (codeTableDataLength !== 0) throw new Error('not implemented: custom code table'); // custom code table
    }

    if (headerIndicator & VCD_APPHEADER) {
      // ignore app header data
      const appDataLength = parser.read7BitEncodedInt();
      parser.skip(appDataLength);
    }
    const headerEndOffset = parser.offset;

    // calculate target file size
    let newFileSize = 0;
    while (!parser.isEOF()) {
      const winHeader = parser.decodeWindowHeader();
      newFileSize += winHeader.targetWindowLength;
      parser.skip(winHeader.addRunDataLength + winHeader.addressesLength + winHeader.instructionsLength);
    }

    const tempFile = new PatchFile(newFileSize);

    parser.seek(headerEndOffset);

    cache = new VCDAddressCache(4, 3);
    const codeTable = VCD_DEFAULT_CODE_TABLE;

    let targetWindowPosition = 0;

    while (!parser.isEOF()) {
      const winHeader = parser.decodeWindowHeader();

      const addRunDataStream = new VCDIFFFile(this.file, parser.offset);
      const instructionsStream = new VCDIFFFile(this.file, addRunDataStream.offset + winHeader.addRunDataLength);
      const addressesStream = new VCDIFFFile(this.file, instructionsStream.offset + winHeader.instructionsLength);

      let addRunDataIndex = 0;

      cache.reset(addressesStream);

      const addressesStreamEndOffset = addressesStream.offset;
      while (instructionsStream.offset < addressesStreamEndOffset) {
        /*
                var instructionIndex=instructionsStream.readS8();
                if(instructionIndex===-1){
                    break;
                }
                */
        const instructionIndex = instructionsStream.readU8();

        for (let i = 0; i < 2; i++) {
          const instruction = codeTable[instructionIndex][i];
          let size = instruction.size;

          if (size === 0 && instruction.type !== 0) {
            size = instructionsStream.read7BitEncodedInt();
          }

          switch (instruction.type) {
            case 0:
              break;
            case VCD_ADD:
              addRunDataStream.copyToFile2(tempFile, addRunDataIndex + targetWindowPosition, size);
              addRunDataIndex += size;
              break;
            case VCD_COPY:
              const addr = cache.decodeAddress(addRunDataIndex + winHeader.sourceLength, instruction.mode);
              let absAddr = 0;

              // source segment and target segment are treated as if they're concatenated
              let sourceData = null;
              if (addr < winHeader.sourceLength) {
                absAddr = winHeader.sourcePosition + addr;
                if (winHeader.indicator & VCD_SOURCE) {
                  sourceData = romFile;
                } else if (winHeader.indicator & VCD_TARGET) {
                  sourceData = tempFile;
                }
              } else {
                absAddr = targetWindowPosition + (addr - winHeader.sourceLength);
                sourceData = tempFile;
              }

              while (size--) {
                if (sourceData) {
                  tempFile.buffer[targetWindowPosition + addRunDataIndex++] = sourceData.buffer[absAddr++];
                }
              }
              break;
            case 2:
              const runByte = addRunDataStream.readU8();
              const offset = targetWindowPosition + addRunDataIndex;
              for (let j = 0; j < size; j++) {
                tempFile.buffer[offset + j] = runByte;
              }

              addRunDataIndex += size;
              break;
            default:
              throw new Error('invalid instruction type found');
          }
        }
      }

      if (
        validate &&
        winHeader.adler32 &&
        winHeader.adler32 !== adler32(tempFile, targetWindowPosition, winHeader.targetWindowLength)
      ) {
        throw new Error('error_crc_output');
      }

      parser.skip(winHeader.addRunDataLength + winHeader.addressesLength + winHeader.instructionsLength);
      targetWindowPosition += winHeader.targetWindowLength;
    }

    return tempFile;
  }
}
