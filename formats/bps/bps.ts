import { crc32 } from '../../crc';
import { PatchFile } from '../../PatchFile';
import { BPSFile } from './BPSFile';
import { BPSNode } from './BPSNode';

const BPS_MAGIC = 'BPS1';
const BPS_ACTION_SOURCE_READ = 0;
const BPS_ACTION_TARGET_READ = 1;
const BPS_ACTION_SOURCE_COPY = 2;
const BPS_ACTION_TARGET_COPY = 3;

interface BpsAction {
  type: number;
  length: number;
  relativeOffset?: number;
  bytes?: number[];
}

export class BPS {
  sourceSize = 0;
  targetSize = 0;
  metaData = '';
  actions: BpsAction[] = [];
  sourceChecksum = 0;
  targetChecksum = 0;
  patchChecksum = 0;

  toString() {
    let s = 'Source size: ' + this.sourceSize;
    s += '\nTarget size: ' + this.targetSize;
    s += '\nMetadata: ' + this.metaData;
    s += '\n#Actions: ' + this.actions.length;
    return s;
  }

  validateSource(romFile: PatchFile, headerSize?: number) {
    return this.sourceChecksum === crc32(romFile, headerSize);
  }

  apply(romFile: PatchFile, validate?: boolean) {
    if (validate && !this.validateSource(romFile)) {
      throw new Error('error_crc_input');
    }

    const tempFile = new PatchFile(this.targetSize);

    // patch
    let sourceRelativeOffset = 0;
    let targetRelativeOffset = 0;
    for (const action of this.actions) {
      if (action.type === BPS_ACTION_SOURCE_READ) {
        romFile.copyToFile(tempFile, tempFile.offset, action.length);
        tempFile.skip(action.length);
      } else if (action.type === BPS_ACTION_TARGET_READ) {
        tempFile.writeBytes(action.bytes as number[]);
      } else if (action.type === BPS_ACTION_SOURCE_COPY) {
        sourceRelativeOffset += action.relativeOffset as number;
        let actionLength = action.length;

        while (actionLength--) {
          tempFile.writeU8(romFile.buffer[sourceRelativeOffset]);
          sourceRelativeOffset++;
        }
      } else if (action.type === BPS_ACTION_TARGET_COPY) {
        targetRelativeOffset += action.relativeOffset as number;
        let actionLength = action.length;
        while (actionLength--) {
          tempFile.writeU8(tempFile.buffer[targetRelativeOffset]);
          targetRelativeOffset++;
        }
      }
    }

    if (validate && this.targetChecksum !== crc32(tempFile)) {
      throw new Error('error_crc_output');
    }

    return tempFile;
  }

  export(fileName?: string) {
    let patchFileSize = BPS_MAGIC.length;

    patchFileSize += BPSFile.getVLVLen(this.sourceSize);
    patchFileSize += BPSFile.getVLVLen(this.targetSize);
    patchFileSize += BPSFile.getVLVLen(this.metaData.length);
    patchFileSize += this.metaData.length;

    for (const action of this.actions) {
      patchFileSize += BPSFile.getVLVLen(((action.length - 1) << 2) + action.type);

      if (action.type === BPS_ACTION_TARGET_READ) {
        patchFileSize += action.length;
      } else if (action.type === BPS_ACTION_SOURCE_COPY || action.type === BPS_ACTION_TARGET_COPY) {
        patchFileSize += BPSFile.getVLVLen(
          (Math.abs(action.relativeOffset as number) << 1) + ((action.relativeOffset as number) < 0 ? 1 : 0),
        );
      }
    }
    patchFileSize += 12;

    const patchFile = new BPSFile(patchFileSize);
    patchFile.fileName = fileName + '.bps';
    patchFile.littleEndian = true;

    patchFile.writeString(BPS_MAGIC);
    patchFile.writeVLV(this.sourceSize);
    patchFile.writeVLV(this.targetSize);
    patchFile.writeVLV(this.metaData.length);
    patchFile.writeString(this.metaData, this.metaData.length);

    for (const action of this.actions) {
      patchFile.writeVLV(((action.length - 1) << 2) + action.type);

      if (action.type === BPS_ACTION_TARGET_READ) {
        patchFile.writeBytes(action.bytes as number[]);
      } else if (action.type === BPS_ACTION_SOURCE_COPY || action.type === BPS_ACTION_TARGET_COPY) {
        patchFile.writeVLV(
          (Math.abs(action.relativeOffset as number) << 1) + ((action.relativeOffset as number) < 0 ? 1 : 0),
        );
      }
    }
    patchFile.writeU32(this.sourceChecksum);
    patchFile.writeU32(this.targetChecksum);
    patchFile.writeU32(this.patchChecksum);

    return patchFile;
  }
}

export function parseBPSFile(file: BPSFile): BPS {
  file.littleEndian = true;
  const patch = new BPS();

  file.seek(4); // skip BPS1

  patch.sourceSize = file.readVLV();
  patch.targetSize = file.readVLV();

  const metaDataLength = file.readVLV();
  if (metaDataLength) {
    patch.metaData = file.readString(metaDataLength);
  }

  const endActionsOffset = file.fileSize - 12;
  while (file.offset < endActionsOffset) {
    const data = file.readVLV();
    const action: BpsAction = { type: data & 3, length: (data >> 2) + 1 };

    if (action.type === BPS_ACTION_TARGET_READ) {
      action.bytes = file.readBytes(action.length);
    } else if (action.type === BPS_ACTION_SOURCE_COPY || action.type === BPS_ACTION_TARGET_COPY) {
      const relativeOffset = file.readVLV();
      action.relativeOffset = (relativeOffset & 1 ? -1 : +1) * (relativeOffset >> 1);
    }

    patch.actions.push(action);
  }

  // file.seek(endActionsOffset);
  patch.sourceChecksum = file.readU32();
  patch.targetChecksum = file.readU32();
  patch.patchChecksum = file.readU32();

  if (patch.patchChecksum !== crc32(file, 0, true)) {
    throw new Error('error_crc_patch');
  }

  return patch;
}

export function createBPSFromFiles(original: PatchFile, modified: PatchFile, deltaMode?: boolean) {
  const patch = new BPS();
  patch.sourceSize = original.fileSize;
  patch.targetSize = modified.fileSize;

  if (deltaMode) {
    patch.actions = createBPSFromFilesDelta(original, modified);
  } else {
    patch.actions = createBPSFromFilesLinear(original, modified);
  }

  patch.sourceChecksum = crc32(original);
  patch.targetChecksum = crc32(modified);
  patch.patchChecksum = crc32(patch.export(), 0, true);
  return patch;
}

/* delta implementation from https://github.com/chiya/beat/blob/master/nall/beat/linear.hpp */
function createBPSFromFilesLinear(original: PatchFile, modified: PatchFile) {
  const patchActions = [];

  /* references to match original beat code */
  const sourceData = original.buffer;
  const targetData = modified.buffer;
  const sourceSize = original.fileSize;
  const targetSize = modified.fileSize;
  const Granularity = 1;

  let targetRelativeOffset = 0;
  let outputOffset = 0;
  let targetReadLength = 0;

  function targetReadFlush() {
    if (targetReadLength) {
      // encode(TargetRead | ((targetReadLength - 1) << 2));
      const action: BpsAction = { type: BPS_ACTION_TARGET_READ, length: targetReadLength, bytes: [] };
      patchActions.push(action);
      let offset = outputOffset - targetReadLength;
      while (targetReadLength) {
        // write(targetData[offset++]);
        if (action.bytes) {
          action.bytes.push(targetData[offset++]);
          targetReadLength--;
        }
      }
    }
  }

  while (outputOffset < targetSize) {
    let sourceLength = 0;
    for (let n = 0; outputOffset + n < Math.min(sourceSize, targetSize); n++) {
      if (sourceData[outputOffset + n] !== targetData[outputOffset + n]) break;
      sourceLength++;
    }

    let rleLength = 0;
    for (let n = 1; outputOffset + n < targetSize; n++) {
      if (targetData[outputOffset] !== targetData[outputOffset + n]) break;
      rleLength++;
    }

    if (rleLength >= 4) {
      // write byte to repeat
      targetReadLength++;
      outputOffset++;
      targetReadFlush();

      // copy starting from repetition byte
      // encode(TargetCopy | ((rleLength - 1) << 2));
      const relativeOffset = outputOffset - 1 - targetRelativeOffset;
      // encode(relativeOffset << 1);
      patchActions.push({ type: BPS_ACTION_TARGET_COPY, length: rleLength, relativeOffset });
      outputOffset += rleLength;
      targetRelativeOffset = outputOffset - 1;
    } else if (sourceLength >= 4) {
      targetReadFlush();
      // encode(SourceRead | ((sourceLength - 1) << 2));
      patchActions.push({ type: BPS_ACTION_SOURCE_READ, length: sourceLength });
      outputOffset += sourceLength;
    } else {
      targetReadLength += Granularity;
      outputOffset += Granularity;
    }
  }

  targetReadFlush();

  return patchActions;
}

/* delta implementation from https://github.com/chiya/beat/blob/master/nall/beat/delta.hpp */
function createBPSFromFilesDelta(original: PatchFile, modified: PatchFile) {
  const patchActions = [];

  /* references to match original beat code */
  const sourceData = original.buffer;
  const targetData = modified.buffer;
  const sourceSize = original.fileSize;
  const targetSize = modified.fileSize;
  const Granularity = 1;

  let sourceRelativeOffset = 0;
  let targetRelativeOffset = 0;
  let outputOffset = 0;

  const sourceTree = new Array(65536);
  const targetTree = new Array(65536);
  for (let n = 0; n < 65536; n++) {
    sourceTree[n] = null;
    targetTree[n] = null;
  }

  // source tree creation
  for (let offset = 0; offset < sourceSize; offset++) {
    let symbol = sourceData[offset];
    // sourceChecksum = crc32_adjust(sourceChecksum, symbol);
    if (offset < sourceSize - 1) symbol |= sourceData[offset + 1] << 8;
    const node = new BPSNode();
    node.offset = offset;
    node.next = sourceTree[symbol];
    sourceTree[symbol] = node;
  }

  let targetReadLength = 0;

  function targetReadFlush() {
    if (targetReadLength) {
      // encode(TargetRead | ((targetReadLength - 1) << 2));
      const action: BpsAction = { type: BPS_ACTION_TARGET_READ, length: targetReadLength, bytes: [] };
      patchActions.push(action);
      let offset = outputOffset - targetReadLength;
      while (targetReadLength) {
        // write(targetData[offset++]);
        if (action.bytes) {
          action.bytes.push(targetData[offset++]);
          targetReadLength--;
        }
      }
    }
  }

  while (outputOffset < modified.fileSize) {
    let maxLength = 0;
    let maxOffset = 0;
    let mode = BPS_ACTION_TARGET_READ;

    let symbol = targetData[outputOffset];
    if (outputOffset < targetSize - 1) symbol |= targetData[outputOffset + 1] << 8;

    {
      // source read
      let length = 0;
      let offset = outputOffset;

      while (offset < sourceSize && offset < targetSize && sourceData[offset] === targetData[offset]) {
        length++;
        offset++;
      }
      if (length > maxLength) {
        maxLength = length;
        mode = BPS_ACTION_SOURCE_READ;
      }
    }

    {
      // source copy
      let node = sourceTree[symbol];
      while (node) {
        let length = 0;
        let x = node.offset;
        let y = outputOffset;

        while (x < sourceSize && y < targetSize && sourceData[x++] === targetData[y++]) length++;
        if (length > maxLength) {
          maxLength = length;
          maxOffset = node.offset;
          mode = BPS_ACTION_SOURCE_COPY;
        }
        node = node.next;
      }
    }

    {
      // target copy
      let node = targetTree[symbol];
      while (node) {
        let length = 0;
        let x = node.offset;
        let y = outputOffset;

        while (y < targetSize && targetData[x++] === targetData[y++]) length++;
        if (length > maxLength) {
          maxLength = length;
          maxOffset = node.offset;
          mode = BPS_ACTION_TARGET_COPY;
        }
        node = node.next;
      }

      // target tree append
      node = new BPSNode();
      node.offset = outputOffset;
      node.next = targetTree[symbol];
      targetTree[symbol] = node;
    }

    {
      // target read
      if (maxLength < 4) {
        maxLength = Math.min(Granularity, targetSize - outputOffset);
        mode = BPS_ACTION_TARGET_READ;
      }
    }

    if (mode !== BPS_ACTION_TARGET_READ) targetReadFlush();

    switch (mode) {
      case BPS_ACTION_SOURCE_READ:
        // encode(BPS_ACTION_SOURCE_READ | ((maxLength - 1) << 2));
        patchActions.push({ type: BPS_ACTION_SOURCE_READ, length: maxLength });
        break;
      case BPS_ACTION_TARGET_READ:
        // delay write to group sequential TargetRead commands into one
        targetReadLength += maxLength;
        break;
      case BPS_ACTION_SOURCE_COPY:
      case BPS_ACTION_TARGET_COPY:
        // encode(mode | ((maxLength - 1) << 2));
        let relativeOffset;
        if (mode === BPS_ACTION_SOURCE_COPY) {
          relativeOffset = maxOffset - sourceRelativeOffset;
          sourceRelativeOffset = maxOffset + maxLength;
        } else {
          relativeOffset = maxOffset - targetRelativeOffset;
          targetRelativeOffset = maxOffset + maxLength;
        }
        // encode((relativeOffset < 0) | (abs(relativeOffset) << 1));
        patchActions.push({ type: mode, length: maxLength, relativeOffset });
        break;
    }

    outputOffset += maxLength;
  }

  targetReadFlush();

  return patchActions;
}
