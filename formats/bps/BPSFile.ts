import { PatchFile } from '../../PatchFile';

export class BPSFile extends PatchFile {
  readVLV() {
    let data = 0;
    let shift = 1;

    while (true) {
      const x = this.readU8();
      data += (x & 0x7f) * shift;
      if (x & 0x80) break;
      shift <<= 7;
      data += shift;
    }

    this._lastRead = data;
    return data;
  }

  writeVLV(data: number) {
    while (true) {
      const x = data & 0x7f;
      data >>= 7;
      if (data === 0) {
        this.writeU8(0x80 | x);
        break;
      }
      this.writeU8(x);
      data--;
    }
  }

  static getVLVLen(data: number) {
    let len = 0;
    while (true) {
      data >>= 7;
      if (data === 0) {
        len++;
        break;
      }
      len++;
      data--;
    }
    return len;
  }
}
