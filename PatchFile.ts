import * as path from 'path';
import * as fs from 'fs';
import * as jszip from 'jszip';
// import * as os from "os";

export class PatchFile {
  littleEndian = false;
  offset: number = 0;

  buffer: Buffer = Buffer.alloc(0);

  _lastRead: any;

  fileName: string = 'new';
  fileSize: number = 0;

  ready: Promise<this> | false = false;

  constructor(source: any, skipHeader?: number) {
    const ctrl = this;
    ctrl.buffer = Buffer.alloc(0);
    // ctrl.littleEndian = (os.endianness() === 'LE');

    if (typeof source === 'object' && typeof source.lastModified === 'number') {
      /* source is File */
      ctrl.initFile(source as File);
    } else if (typeof source === 'string') {
      /* source is string */
      ctrl.initString(source as string);
    } else if (
      typeof source === 'object' &&
      typeof source.fileName === 'string' &&
      typeof source.littleEndian === 'boolean'
    ) {
      /* source is PatchFile */
      ctrl.initPatchFile(source as PatchFile);
    } else if (typeof source === 'object' && typeof source.byteLength === 'number') {
      /* source is Buffer */
      ctrl.initBuffer(source as Buffer);
    } else if (typeof source === 'number') {
      /* source is number */
      ctrl.initNumber(source as number);
    } else {
      throw new Error('Invalid source');
    }

    if (this.ready) {
      this.ready.finally(() => {
        if (skipHeader) {
          this.buffer = this.buffer.slice(skipHeader);
          this.fileSize = this.buffer.byteLength;
        }
      });
    }
  }

  private initFile(source: File) {
    const bufferPromise = source.arrayBuffer();

    if (source.name.endsWith('.zip')) {
      this.ready = bufferPromise.then((buffer: ArrayBuffer) => {
        return this.unzipContents(Buffer.from(buffer), source.name);
      });
    } else {
      this.fileName = source.name;
      this.ready = bufferPromise.then((buffer: ArrayBuffer) => {
        this.buffer = Buffer.from(buffer);
        this.fileSize = buffer.byteLength;
        return this;
      });
    }
  }

  private initString(source: string) {
    if (source.endsWith('.zip')) {
      this.ready = this.unzipContents(fs.readFileSync(source), path.basename(source));
    } else {
      this.fileName = path.basename(source);
      this.buffer = fs.readFileSync(source);
      this.fileSize = this.buffer.length;
    }
  }

  private initPatchFile(source: PatchFile) {
    const _source = source as PatchFile;
    this.fileName = _source.fileName;
    this.fileSize = _source.fileSize;

    this.buffer = _source.buffer;

    this.ready = source.copyToFile(this, 0).then(() => {
      return this;
    });
  }

  private initNumber(source: number) {
    this.fileName = 'file.bin';
    this.fileSize = source;
    this.buffer = Buffer.alloc(source);
  }

  private initBuffer(source: Buffer) {
    this.fileName = 'file.bin';
    this.fileSize = source.byteLength;
    this.buffer = source;
  }

  async unzipContents(source: Buffer, name: string) {
    function escapeRegExp(str: string) {
      return str.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
    }

    const filename = escapeRegExp(name.slice(0, -4));

    const contents = await jszip.loadAsync(source);
    const romfile = contents.file(new RegExp(`${filename}\..*`))[0];

    if (romfile) {
      const buffer = await romfile.async('nodebuffer');

      this.fileName = romfile.name;
      this.buffer = buffer;
      this.fileSize = this.buffer.length;
      return this;
    } else {
      throw new Error("Couldn't locate ROM in ZIP.");
    }
  }

  seek(offset: number) {
    this.offset = offset;
  }

  skip(nBytes: number) {
    this.offset += nBytes;
  }

  isEOF(): boolean {
    return !(this.offset < this.fileSize);
  }

  slice(offset: number, len: number): PatchFile {
    len = len || this.fileSize - offset;

    let newFile;

    if (typeof this.buffer.slice !== 'undefined') {
      newFile = new PatchFile(0);
      newFile.fileSize = len;
      newFile.buffer = new Buffer(this.buffer.slice(offset, offset + len));
    } else {
      newFile = new PatchFile(len);
      this.copyToFile(newFile, offset, len, 0);
    }
    newFile.fileName = this.fileName;
    newFile.littleEndian = this.littleEndian;

    return newFile;
  }

  async copyToFile(target: PatchFile, offsetSource: number, len?: number, offsetTarget?: number) {
    if (typeof offsetTarget === 'undefined') offsetTarget = offsetSource;

    len = len || this.fileSize - offsetSource;

    for (let i = 0; i < len; i++) {
      target.buffer[offsetTarget + i] = this.buffer[offsetSource + i];
    }
  }

  save(name?: string) {
    fs.writeFileSync(name || this.fileName, this.buffer);
  }

  readU8() {
    this._lastRead = this.buffer[this.offset];

    this.offset++;
    return this._lastRead;
  }

  readU16() {
    if (this.littleEndian) this._lastRead = this.buffer.readUInt16LE(this.offset);
    else this._lastRead = this.buffer.readUInt16BE(this.offset);

    this.offset += 2;
    return this._lastRead >>> 0;
  }

  readU24() {
    if (this.littleEndian)
      this._lastRead =
        this.buffer[this.offset] + (this.buffer[this.offset + 1] << 8) + (this.buffer[this.offset + 2] << 16);
    else
      this._lastRead =
        (this.buffer[this.offset] << 16) + (this.buffer[this.offset + 1] << 8) + this.buffer[this.offset + 2];

    this.offset += 3;
    return this._lastRead >>> 0;
  }

  readU32() {
    if (this.littleEndian) this._lastRead = this.buffer.readUInt32LE(this.offset);
    else this._lastRead = this.buffer.readUInt32BE(this.offset);

    this.offset += 4;
    return this._lastRead >>> 0;
  }

  readBytes(len: number) {
    this._lastRead = new Array(len);
    for (let i = 0; i < len; i++) {
      this._lastRead[i] = this.buffer[this.offset + i];
    }

    this.offset += len;
    return this._lastRead;
  }

  readString(len: number) {
    this._lastRead = '';
    for (let i = 0; i < len && this.offset + i < this.fileSize && this.buffer[this.offset + i] > 0; i++)
      this._lastRead = this._lastRead + String.fromCharCode(this.buffer[this.offset + i]);

    this.offset += len;
    return this._lastRead;
  }

  writeU8(u8: number) {
    this.buffer[this.offset] = u8;

    this.offset++;
  }

  writeU16(u16: number) {
    if (this.littleEndian) {
      this.buffer.writeUInt16LE(u16, this.offset);
    } else {
      this.buffer.writeUInt16BE(u16, this.offset);
    }

    this.offset += 2;
  }

  writeU24(u24: number) {
    if (this.littleEndian) {
      this.buffer[this.offset] = u24 & 0x0000ff;
      this.buffer[this.offset + 1] = (u24 & 0x00ff00) >> 8;
      this.buffer[this.offset + 2] = (u24 & 0xff0000) >> 16;
    } else {
      this.buffer[this.offset] = (u24 & 0xff0000) >> 16;
      this.buffer[this.offset + 1] = (u24 & 0x00ff00) >> 8;
      this.buffer[this.offset + 2] = u24 & 0x0000ff;
    }

    this.offset += 3;
  }

  writeU32(u32: number) {
    if (this.littleEndian) {
      this.buffer.writeUInt32LE(u32, this.offset);
    } else {
      this.buffer.writeUInt32BE(u32, this.offset);
    }

    this.offset += 4;
  }

  writeBytes(a: number[]) {
    for (let i = 0; i < a.length; i++) this.buffer[this.offset + i] = a[i];

    this.offset += a.length;
  }

  writeString(str: string, len?: number) {
    len = len || str.length;
    let i = 0;
    for (; i < str.length && i < len; i++) this.buffer[this.offset + i] = str.charCodeAt(i);

    for (; i < len; i++) this.buffer[this.offset + i] = 0x00;

    this.offset += len;
  }
}
