import { PatchFile } from './PatchFile';
import { ProcessFile } from './ProcessFile';

import { UPSFile } from './formats/ups/UPSFile';
import { BPSFile } from './formats/bps/BPSFile';

import { UPS, parseUPSFile, createUPSFromFiles } from './formats/ups/ups';
import { BPS, createBPSFromFiles, parseBPSFile } from './formats/bps/bps';
import { createIPSFromFiles, IPS, parseIPSFile } from './formats/ips';
import { parseVCDIFF, VCDIFFFile } from './formats/vcdiff/VCDIFFFile';
import { VCDIFF } from './formats/vcdiff/VCDIFF';

class RomPatcher {
  inputFiles: ProcessFile[] = [];
  patchFile: IPS | UPS | BPS | VCDIFF | null = null;
  format: string = '';

  async preparePatch(
    original: PatchFile,
    patch: PatchFile,
    format: string,
    validate: boolean = true,
  ): Promise<PatchFile> {
    this.format = format;
    this.inputFiles = [];

    if (original.ready) {
      await original.ready;
    }

    this.inputFiles.push(new ProcessFile(original));

    switch (format) {
      case 'ips':
        this.patchFile = parseIPSFile(new PatchFile(patch));
        return this.patchFile.apply(original);
      case 'ups':
        this.patchFile = parseUPSFile(new UPSFile(patch));
        return this.patchFile.apply(original, validate);
      case 'bps':
        this.patchFile = parseBPSFile(new BPSFile(patch));
        return this.patchFile.apply(original, validate);
      case 'vcdiff':
      case 'xdelta':
        this.patchFile = parseVCDIFF(new VCDIFFFile(patch));
        return this.patchFile.apply(original, validate);
      default:
        throw new Error('Invalid patch format.');
    }
  }

  async createPatch(original: PatchFile, modified: PatchFile, format: string): Promise<IPS | UPS | BPS> {
    // Wait for unzipping & buffers to be read
    if (original.ready && modified.ready) await Promise.all([original.ready, modified.ready]);

    switch (format) {
      case 'ips':
        return createIPSFromFiles(original, modified);
      case 'ups':
        return createUPSFromFiles(original, modified);
      case 'bps':
        return createBPSFromFiles(original, modified);
      default:
        throw new Error('Invalid patch format.');
    }
  }

  isValid(): boolean {
    switch (this.format) {
      case 'ups':
      case 'bps':
        // @ts-ignore
        return this.patchFile.validateSource(this.inputFiles[0].file);
    }
    return false;
  }
}

export { RomPatcher, PatchFile, ProcessFile };
