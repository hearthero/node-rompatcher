/* CRC32 - from Alex - https://stackoverflow.com/a/18639999 */
import { PatchFile } from './PatchFile';

const CRC32_TABLE = (() => {
  let c;
  const crcTable = [];
  for (let n = 0; n < 256; n++) {
    c = n;
    for (let k = 0; k < 8; k++) c = c & 1 ? 0xedb88320 ^ (c >>> 1) : c >>> 1;
    crcTable[n] = c;
  }
  return crcTable;
})();

export function crc32(patchFile: PatchFile, headerSize?: number, ignoreLast4Bytes?: boolean) {
  const data = headerSize ? new Uint8Array(patchFile.buffer, headerSize) : patchFile.buffer;

  let crc = 0 ^ -1;

  const len = ignoreLast4Bytes ? data.length - 4 : data.length;
  for (let i = 0; i < len; i++) crc = (crc >>> 8) ^ CRC32_TABLE[(crc ^ data[i]) & 0xff];

  return (crc ^ -1) >>> 0;
}

export function padZeroes(intVal: number, nBytes: number) {
  let hexString = intVal.toString(16);
  while (hexString.length < nBytes * 2) hexString = '0' + hexString;
  return hexString;
}

/* Adler-32 - https://en.wikipedia.org/wiki/Adler-32#Example_implementation */

const ADLER32_MOD = 0xfff1;

export function adler32(patchFile: PatchFile, offset: number, len: number) {
  let a = 1;
  let b = 0;

  for (let i = 0; i < len; i++) {
    a = (a + patchFile.buffer[i + offset]) % ADLER32_MOD;
    b = (b + a) % ADLER32_MOD;
  }

  return ((b << 16) | a) >>> 0;
}
