import { PatchFile } from './PatchFile';
import { UPSFile } from './formats/ups/UPSFile';
import { BPSFile } from './formats/bps/BPSFile';
import { createHash } from 'crypto-browserify';
import { crc32 } from './crc';

export class ProcessFile {
  file: PatchFile | UPSFile | BPSFile;

  crc32 = 0;
  md5 = createHash('md5');
  sha1 = createHash('sha1');

  constructor(file: PatchFile, slice?: number) {
    this.file = file;
    if (slice) {
      this.file.buffer = this.file.buffer.slice(0, slice);
    }
    this.processChecksums();
  }

  processChecksums() {
    this.crc32 = crc32(this.file);
    this.md5 = this.md5.update(this.file.buffer);
    this.sha1 = this.sha1.update(this.file.buffer);
  }

  getChecksums() {
    return {
      crc32: this.crc32.toString(16),
      md5: this.md5.digest('hex'),
      sha1: this.sha1.digest('hex'),
    };
  }
}
